const faker = require('faker');

module.exports = (author) => {
  return {
    title: faker.lorem.sentence(),
    content: faker.lorem.paragraphs(),
    author,
  };
};
