const express = require('express');
const router = express.Router();
const Book = require('../../models/Book');
const auth = require('../../middlewares/auth');

router.get('/', async (req, res) => {
  try {
    const books = await Book.find({});
    for (const book of books) {
      await book.populate('author', 'account').execPopulate();
    }
    return res.json({status: 'success', result: books});
  } catch (error) {
    res.status(500).send({status: 'fail', error: error.message});
  }
});

router.get('/:id', async (req, res) => {
  try {
    const id = req.params.id
    const book = await Book.findOne({_id: id});
    return res.json({status: 'success', result: book});
  } catch (error) {
    res.status(500).send({status: 'fail', error: error.message});
  }
});

router.post('/', auth, async (req, res) => {
  try {
    const { title, content } = req.body;
    const author = req.user;

    if ( !title || typeof title !== 'string' ) {
      return res.status(400).json({status: 'fail', error: 'title required'})
    }

    if ( !content || typeof content !== 'string' ) {
      return res.status(400).json({status: 'fail', error: 'content required'})
    }

    const book = await new Book({title, content, author}).save();
    await book.populate('author', 'account').execPopulate();
    return res.json({status: 'success', result: book});
  } catch (error) {
    res.status(500).send({status: 'fail', error: error.message});
  }
});

router.put('/:id', auth, async (req, res) => {
  try {
    const { title, content } = req.body;
    const id = req.params.id
    const book = await Book.findOne({_id: id});
    book.title = title;
    book.content = content;
    await book.save();
    await book.populate('author', 'account').execPopulate();
    return res.json({status: 'success', result: book});
  } catch (error) {
    res.status(500).send({status: 'fail', error: error.message});
  }
});

router.delete('/:id', auth, async (req, res) => {
  try {
    const id = req.params.id
    const book = await Book.deleteOne({_id: id});
    return res.json({status: 'success', result: book});
  } catch (error) {
    res.status(500).send({status: 'fail', error: error.message});
  }
});

module.exports = router;
