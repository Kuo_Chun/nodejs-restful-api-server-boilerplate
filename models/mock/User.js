const faker = require('faker');

module.exports = () => {
  return {
    account: faker.internet.userName(),
    password: faker.internet.password(),
  };
};
