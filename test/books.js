const app = require('../app');
const chai = require('chai');
const chaiHttp = require('chai-http');
const { expect } = chai;
chai.use(chaiHttp);

const writeMockData2DB = require('../writeMockData2DB');
const MockBook = require('../models/mock/Book');
const Book = require('../models/Book');
const User = require('../models/User');
let mockData;

describe('API books', () => {
  before(async () => {
    mockData = await writeMockData2DB();
  })

  describe('GET /api/books', () => {
    it('respond with an array of books', (done) => {
      chai
        .request(app)
        .get('/api/books')
        .end(async (err, res) => {
          expect(res).to.have.status(200);
          expect(res.body.status).to.equals('success');
          const books = await Book.find({});
          expect(res.body.result).to.lengthOf(books.length);
          books.forEach((book, i) => {
            expect(res.body.result[i]._id).to.equals(book._id.toString());
            expect(res.body.result[i].title).to.equals(book.title);
            expect(res.body.result[i].content).to.equals(book.content);
          });
          done();
        });
    });
  });

  describe('GET /api/books/:id', () => {
    it('respond with an object of book', (done) => {
      const id = mockData.books[0]._id.toString();
      chai
        .request(app)
        .get(`/api/books/${id}`)
        .end((err, res) => {
          expect(res).to.have.status(200);
          expect(res.body.status).to.equals('success');
          expect(res.body.result._id).to.equals(mockData.books[0]._id.toString());
          expect(res.body.result.title).to.equals(mockData.books[0].title);
          expect(res.body.result.content).to.equals(mockData.books[0].content);
          done();
        });
    });
  });

  describe('POST /api/books', () => {
    it('respond with an object of book', (done) => {
      const newBook = MockBook(mockData.users[0]._id.toString());
      chai
        .request(app)
        .post('/api/books')
        .set('Authorization', mockData.users[0].tokens[0].token)
        .send(newBook)
        .end((err, res) => {
          expect(res).to.have.status(200);
          expect(res.body.status).to.equals('success');
          expect(res.body.result.title).to.equals(newBook.title);
          expect(res.body.result.content).to.equals(newBook.content);
          expect(res.body.result.author._id).to.equals(newBook.author);
          done();
        });
    });

    it('respond with not authorized', (done) => {
      const newBook = MockBook(mockData.users[0]._id.toString());
      chai
        .request(app)
        .post('/api/books')
        .send(newBook)
        .end((err, res) => {
          expect(res).to.have.status(401);
          expect(res.body.status).to.equals('fail');
          expect(res.body.error).to.equals('not authorized');
          done();
        });
    });

    it('respond with title required', (done) => {
      const newBook = MockBook(mockData.users[0]._id.toString());
      chai
        .request(app)
        .post('/api/books')
        .set('Authorization', mockData.users[0].tokens[0].token)
        .send({content: newBook.content})
        .end((err, res) => {
          expect(res).to.have.status(400);
          expect(res.body.status).to.equals('fail');
          expect(res.body.error).to.equals('title required');
          done();
        });
    });

    it('respond with content required', (done) => {
      const newBook = MockBook(mockData.users[0]._id.toString());
      chai
        .request(app)
        .post('/api/books')
        .set('Authorization', mockData.users[0].tokens[0].token)
        .send({title: newBook.title})
        .end((err, res) => {
          expect(res).to.have.status(400);
          expect(res.body.status).to.equals('fail');
          expect(res.body.error).to.equals('content required');
          done();
        });
    });
  });

  describe('PUT /api/books', () => {
    it('respond with an object of book', (done) => {
      const id = mockData.books[0]._id.toString();
      const newBook = MockBook(mockData.users[0]._id.toString());
      chai
        .request(app)
        .put(`/api/books/${id}`)
        .set('Authorization', mockData.users[0].tokens[0].token)
        .send(newBook)
        .end((err, res) => {
          expect(res).to.have.status(200);
          expect(res.body.status).to.equals('success');
          expect(res.body.result.title).to.equals(newBook.title);
          expect(res.body.result.content).to.equals(newBook.content);
          expect(res.body.result.author._id).to.equals(newBook.author);
          done();
        });
    });

    it('respond with not authorized', (done) => {
      const id = mockData.books[0]._id.toString();
      const newBook = MockBook(mockData.users[0]._id.toString());
      chai
        .request(app)
        .put(`/api/books/${id}`)
        .send(newBook)
        .end((err, res) => {
          expect(res).to.have.status(401);
          expect(res.body.status).to.equals('fail');
          expect(res.body.error).to.equals('not authorized');
          done();
        });
    });
  });

  describe('DELETE /api/books/:id', () => {
    it('respond with success', (done) => {
      const id = mockData.books[0]._id.toString();
      chai
        .request(app)
        .del(`/api/books/${id}`)
        .set('Authorization', mockData.users[0].tokens[0].token)
        .end(async (err, res) => {
          expect(res).to.have.status(200);
          expect(res.body.status).to.equals('success');
          let book = await Book.findOne({_id: id});
          expect(book).to.equals(null);
          done();
        });
    });

    it('respond with not authorized', (done) => {
      const id = mockData.books[0]._id.toString();
      chai
        .request(app)
        .del(`/api/books/${id}`)
        .end(async (err, res) => {
          expect(res).to.have.status(401);
          expect(res.body.status).to.equals('fail');
          expect(res.body.error).to.equals('not authorized');
          done();
        });
    });
  });
});
