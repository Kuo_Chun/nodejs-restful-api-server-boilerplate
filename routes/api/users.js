const express = require('express');
const router = express.Router();
const User = require('../../models/User')

router.post('/register', async (req, res) => {
  try {
    const { account, password } = req.body;

    if ( !account || typeof account !== 'string' ) {
      return res.status(400).json({status: 'fail', error: 'account required'});
    }

    if ( !password || typeof password !== 'string' ) {
      return res.status(400).json({status: 'fail', error: 'password required'});
    }

    const user = await User({account, password}).save();
    const token = await user.generateAuthToken();
    return res.json({status: 'success', token});
  } catch ( error ) {
    res.status(500).send({status: 'fail', error: error.message});
  }
});

router.post('/login', async (req, res) => {
  try {
    const { account, password } = req.body;

    if ( !account || typeof account !== 'string' ) {
      return res.status(400).json({status: 'fail', error: 'account required'});
    }

    if ( !password || typeof password !== 'string' ) {
      return res.status(400).json({status: 'fail', error: 'password required'});
    }

    const user = await User.findByCredentials(account, password)
    if ( !user ) {
      return res.status(401).send({status: 'fail', error: 'login failed'});
    }
    const token = await user.generateAuthToken();
    res.send({status: 'success', token });
  } catch ( error ) {
    res.status(500).send({status: 'fail', error: error.message});
  }
});

module.exports = router;
