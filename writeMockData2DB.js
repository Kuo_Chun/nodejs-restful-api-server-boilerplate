const Book = require('./models/Book');
const MockBook = require('./models/mock/Book');
const User = require('./models/User');
const MockUser = require('./models/mock/User');

module.exports = () => {
  return new Promise(async (resolve, reject) => {
    try {
      const booksData = [];
      let users = [];
      let originUsers = []
      let books;

      for ( let i = 0 ; i < 10 ; i++ ) {
        let user = MockUser();
        originUsers.push(user);
        const _user = await User(user).save();
        await _user.generateAuthToken();
        users.push(_user);
      }

      for ( let i = 0 ; i < 10 ; i++ ) {
        for ( let j = 0 ; j < 10 ; j++ ) {
          booksData.push(MockBook(users[i]._id));
        }
      }

      books = await Book.insertMany(booksData);

      resolve({
        users,
        books,
        originUsers,
      });
    } catch(error) {
      reject(error);
    }
  });
};
