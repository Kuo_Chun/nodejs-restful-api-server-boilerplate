# Nodejs RESTful API Server Boilerplate

這是一個Node.js RESTful API Server的樣版專案

> Server框架採用Express

> 資料庫採用MongoDB，ODM為Mongoose

> API認證方式採用JWT驗證

> 密碼加密方式採用bcrypt演算法

> 自動測試使用mocha

> mock資料使用faker產生


## Installation

```bash
$ npm install
```

## Start Server

```bash
$ DEBUG=api-server:* npm start
```

## Start Test

自動測試會呼叫writeMockData2DB，產生mock data

```bash
$ npm test
```

## 資料庫設定
資料庫採用MongoDB，ODM為Mongoose

Config設定在/config/default.json>dbConfig

## authentication, authorization說明
密碼加密方式採用bcrypt演算法，saltRounds設定在/config/default.json>authConfig>saltRounds

認證方式採用JWT驗證，JWT key設定在/config/default.json>authConfig>jwtKey

## 產生mock data方式
使用faker進行亂數產生資料

mock資料模型定義在/models/mock

產生過程寫在writeMockData2DB

## Restful API URL
GET /api/books

GET /api/books/:id

POST /api/books

PUT /api/books

DELETE /api/books/:id

POST /api/users/register

POST /api/users/login