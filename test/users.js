const app = require('../app');
const chai = require('chai');
const chaiHttp = require('chai-http');
const { expect } = chai;
chai.use(chaiHttp);

const writeMockData2DB = require('../writeMockData2DB');
const MockUser = require('../models/mock/User');
const User = require('../models/User');
const newUser = MockUser();
let mockData;

describe('API users', () => {
  before(async () => {
    mockData = await writeMockData2DB();
  })

  describe('POST /api/users/register', () => {
    it('register a user', (done) => {
      chai
        .request(app)
        .post('/api/users/register')
        .send(newUser)
        .end(async (err, res) => {
          expect(res).to.have.status(200);
          expect(res.body.status).to.equals('success');
          const user = await User.findOne({account: newUser.account});
          const isPasswordMatch = await user.isPasswordMatch(newUser.password);
          expect(isPasswordMatch).to.equals(true);
          done();
        });
    });

    it('respond with account required', (done) => {
      chai
        .request(app)
        .post('/api/users/register')
        .send({password: newUser.password})
        .end((err, res) => {
          expect(res).to.have.status(400);
          expect(res.body.status).to.equals('fail');
          expect(res.body.error).to.equals('account required');
          done();
        });
    });

    it('respond with password required', (done) => {
      chai
        .request(app)
        .post('/api/users/register')
        .send({account: newUser.account})
        .end((err, res) => {
          expect(res).to.have.status(400);
          expect(res.body.status).to.equals('fail');
          expect(res.body.error).to.equals('password required');
          done();
        });
    });
  });

  describe('POST /api/users/login', () => {
    it('login success', (done) => {
      chai
        .request(app)
        .post('/api/users/login')
        .send(mockData.originUsers[0])
        .end(async (err, res) => {
          expect(res).to.have.status(200);
          expect(res.body.status).to.equals('success');
          const tokens = mockData.users[0].tokens;
          expect(res.body.token).to.equals(tokens[tokens.length-1].token);
          done();
        });
    });

    it('login fail', (done) => {
      chai
        .request(app)
        .post('/api/users/login')
        .send({password: mockData.originUsers[0].password})
        .end((err, res) => {
          expect(res).to.have.status(400);
          expect(res.body.status).to.equals('fail');
          expect(res.body.error).to.equals('account required');
          done();
        });
    });
  });
});
