const jwt = require('jsonwebtoken');
const User = require('../models/User');
const config = require('config');
const authConfig = config.get('Boilerplate.authConfig');

const auth = async(req, res, next) => {
  try {
    const authorization = req.header('Authorization') || '';
    const token = authorization.replace('Bearer ', '');
    const data = jwt.verify(token, authConfig.jwtKey);
    const user = await User.findOne({ _id: data._id, 'tokens.token': token });
    if ( !user ) {
      throw new Error();
    }
    req.user = user;
    req.token = token;
    next();
  } catch ( error ) {
    res.status(401).json({status: 'fail', error: 'not authorized' })
  }
};

module.exports = auth;
